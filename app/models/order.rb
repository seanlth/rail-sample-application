class Order < ApplicationRecord
	belongs_to :data_table, optional: true
	belongs_to :level, optional: true
end
