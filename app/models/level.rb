class Level < ApplicationRecord
	has_many :orders
	belongs_to :order_books, optional: true
	
end
