class DataTable < ApplicationRecord
	has_many :orders	
	#serialize :table
	#attr_accessor :flag
	attr_accessor :buy
	attr_accessor :sell
	

	def read_data(n, range)
		data = Array.new
		(0...n).each do |index|
			if rand(2) == 0
				data.push [index, "instrument", "buy", rand(range), rand(range)]
			else 
				data.push [index, "instrument", "sell", rand(range), rand(range)]
			end				
		end

		data
	end

	def populate(n, range)
		#puts orders.exists?(price: 4)
		#orders.clear
		if orders.size == 0
			data = read_data(n, range)
				
			data.each do |d| 
				#order = Order.new( order_id: d[0], instrument: d[1], side: d[2], price: d[3], quantity: d[4] )
				#order.save
				orders.create( { order_id: d[0], instrument: d[1], side: d[2], price: d[3], quantity: d[4] } )
				#puts order.present?
				
			end
		end
		#puts orders.size
		#puts Order.first

		#order = Order.new( order_id: 45345798, instrument: "", side: "", price: 4, quantity: 4 )
		#order.save
		#puts Order.exists?(price: 4)
		#puts Order.exists?(:price => 4)
		#puts orders.exists?(price: 4)
		#puts Order.find_or_create_by(order_id: 45345798)
		#puts Order.exists?(order_id: 0)		
		#puts Order.find_or_create_by(order_id: 45345798)		
	end

	def organise(sort_by_price_or_id)
		@buy = orders.select { |order| order.side == "buy" }
		@sell = orders.select { |order| order.side == "sell" }	
		
		if sort_by_price_or_id == true 
			@buy.sort_by! { |order| order.price }
			@sell.sort_by! { |order| order.price }
			
		else 
			@buy.sort_by! { |order| order.order_id }	
			@sell.sort_by! { |order| order.order_id }			
		end
	end


end
