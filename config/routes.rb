Rails.application.routes.draw do
  	get 'welcome/index'

  	resources :articles do 
		resources :comments
  	end
	
	get 'data_table/', to: 'data_table#show'	
	resources :data_table do
		#resources :orders
	end


	get 'order_books/', to: 'order_books#show'	
	resources :order_books do
		resources :levels do 
			resources :orders 
		end
	end


  	root 'welcome#index'
  	# For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
