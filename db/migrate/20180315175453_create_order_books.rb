class CreateOrderBooks < ActiveRecord::Migration[5.1]
  def change
    create_table :order_books do |t|
      t.string :instrument

      t.timestamps
    end
  end
end
