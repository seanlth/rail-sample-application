class CreateLevels < ActiveRecord::Migration[5.1]
	def change
		create_table :levels do |t|
			t.integer :price
			t.string :side
			t.references :order_book, foreign_key: true
			
			t.timestamps
		end
	end
end
