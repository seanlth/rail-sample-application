class CreateOrders < ActiveRecord::Migration[5.1]
	def change
		create_table :orders do |t|
			t.integer :order_id
			t.string :instrument
			t.string :side
			t.integer :price
			t.integer :quantity
			t.references :data_table, foreign_key: true
			t.references :level, foreign_key: true
			t.timestamps
		end
	end
end
