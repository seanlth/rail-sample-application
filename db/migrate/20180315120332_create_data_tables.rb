class CreateDataTables < ActiveRecord::Migration[5.1]
  def change
    create_table :data_tables do |t|
      t.string :name
	  t.boolean :flag
      t.timestamps
    end
  end
end
