require 'test_helper'

class DataTableTest < ActiveSupport::TestCase
	test "data source" do
		@data_table = DataTable.new(name: "TestTable")
		@data_table.populate(10, 100)
		@data_table.organise(true)

		assert @data_table.buy.length + @data_table.sell.length == 10
	end

	test "sort data by index" do
		@data_table = DataTable.new(name: "TestTable")
		@data_table.populate(10, 100)
		@data_table.organise(false)
			
		(1...@data_table.buy.length).each do |index| 
			assert @data_table.buy[index].order_id > @data_table.buy[index-1].order_id
		end
	end

	test "sort data by value" do
		@data_table = DataTable.new(name: "TestTable")
		@data_table.populate(10, 100)
		@data_table.organise(true)
	
		(1...@data_table.sell.length).each do |index| 
			assert @data_table.sell[index].price >= @data_table.sell[index-1].price
		end
	end
end
